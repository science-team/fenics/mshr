import os, sys
import subprocess
import json

from setuptools import setup, Extension


# Call cmake to generate json file with include and link information about dolfin and pybind11
if not os.path.isfile(os.path.join("build", "config.json")) :
    if not os.path.exists("build") :
        os.mkdir("build")
    cmake_command=["cmake", os.getcwd()]
    if os.environ.get('CMAKE_PREFIX_PATH'):
        cmake_command.extend(['-DCMAKE_PREFIX_PATH={}'.format(os.environ['CMAKE_PREFIX_PATH'])])
    subprocess.check_call(cmake_command, cwd=os.path.abspath("build"))

with open(os.path.join("build", "config.json"), 'r') as infile :
    config = json.load(infile)

include_dirs = config["pybind11"]["include_dirs"].split(";") + \
               config["dolfin"]["include_dirs"].split(";")   + \
               config["mshr"]["include_dirs"].split(";")


mshr_ext = Extension('mshr.cpp',
                     ['src/mshr.cpp'],
                     include_dirs=include_dirs,
                     library_dirs=config['mshr']['lib_dirs'].split(";"),
                     libraries=config['mshr']['libs'].split(";"),
                     extra_compile_args=['-std=c++14'],
                     language='c++14')


setup(name             = 'mshr',
      version          = '2019.2.0.dev0',
      author           = 'FEniCS Project',
      description      = 'mshr python interface (via pybind11)',
      long_description = '',
      packages         = ["mshr",],
      ext_modules      = [mshr_ext],
      install_requires = ["numpy", "fenics-dolfin"]
      #zip_safe         = False)
      )
